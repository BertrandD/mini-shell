/*--------------------------------------------------------------------------
 * headers à inclure afin de pouvoir utiliser divers appels systèmes
 * -----------------------------------------------------------------------*/
#include <stdio.h>     // pour printf() and co
#include <stdlib.h>    // pour exit()
#include <errno.h>     // pour errno and co
#include <unistd.h>    // pour pipe
#include <sys/types.h> // pour avoir pid_t
#include <signal.h>    // pour sigaction
#include <string.h>    // pour avoir strcmp and co

#include <sys/wait.h>  // pour avoir wait and co

#include "jobs.h"
#include "externes.h"



/*--------------------------------------------------------------------------
 * crée un fils pour exécuter la commande ligne_analysee->commandes[num_comm]
 * enregistre son pid dans job->pids[num_comm]
 * le fils définit ses handlers à différents signaux grâce à sig
 * -----------------------------------------------------------------------*/
static void execute_commande_dans_un_fils(job_t *job,int num_comm, ligne_analysee_t *ligne_analysee, struct sigaction *sig)
{
	if(num_comm<ligne_analysee->nb_fils-1)
	{
		if (pipe(job->tubes[num_comm])==-1)
	    	{perror("Echec création tube"); exit(errno);}  
	}

	pid_t resFork = fork();
    if (resFork==-1) {perror("Echec fork ");}
	if(resFork==0)
	{	

		sig->sa_handler=SIG_DFL;
      	sigaction(SIGINT,sig,NULL);

		if(num_comm==0 && ligne_analysee->nb_fils>1)
		{
	   		dup2(job->tubes[num_comm][1], STDOUT_FILENO);
	    	close(job->tubes[num_comm][0]); 
		 	close(job->tubes[num_comm][1]); 
		}else if(num_comm<ligne_analysee->nb_fils-1 && ligne_analysee->nb_fils>1){
    		dup2(job->tubes[num_comm-1][0], STDIN_FILENO);
    		dup2(job->tubes[num_comm][1], STDOUT_FILENO);
	    	close(job->tubes[num_comm-1][0]); 
	    	close(job->tubes[num_comm][0]); 
		 	close(job->tubes[num_comm][1]); 
    	}else if(num_comm==ligne_analysee->nb_fils-1){
	   		dup2(job->tubes[num_comm-1][0], STDIN_FILENO);
        	close(job->tubes[num_comm-1][0]); 
		}
    	


		sig->sa_handler=SIG_DFL;
      	sigaction(SIGINT,sig,NULL);
		int res=execvp(ligne_analysee->commandes[num_comm][0],ligne_analysee->commandes[num_comm]);
	    if (res==-1) {perror("Commande inconnue ");  exit(errno);}
	}else{
		job->pids[num_comm]=resFork;
	}

	if(ligne_analysee->nb_fils>1 && num_comm<ligne_analysee->nb_fils-1)
	{
		close(job->tubes[num_comm][1]);
	}
	
	if(num_comm>1 && ligne_analysee->nb_fils>1)
		close(job->tubes[num_comm-1][1]);

	
}
/*--------------------------------------------------------------------------
 * Fait exécuter les commandes de la ligne par des fils
 * -----------------------------------------------------------------------*/
void executer_commandes(job_t *job, ligne_analysee_t *ligne_analysee, struct sigaction *sig)
{
  // recopie de la ligne de commande dans la structure job
  strcpy(job->nom,ligne_analysee->ligne);

  job->nb_restants = ligne_analysee->nb_fils;

  // on lance l'exécution de la commande dans un fils
  for (int i = 0; i < ligne_analysee->nb_fils; ++i)
  {
	  execute_commande_dans_un_fils(job,i,ligne_analysee, sig);
  }


  // TODO : à compléter
/*  for (int i = 0; i < ligne_analysee->nb_fils; ++i)
  {
	  waitpid(job->pids[i],NULL,0);
  }*/

  // on ne se sert plus de la ligne : ménage
  *ligne_analysee->ligne='\0';
}


